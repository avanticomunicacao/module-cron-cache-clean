<?php
declare(strict_types=1);

namespace Avanti\CronCacheClean\Cron;

use Magento\Framework\App\Cache\TypeList as CacheTypeList;
use Psr\Log\LoggerInterface;

class ClearCache
{

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var CacheTypeList
     */
    protected $cache;


    /**
     * Constructor
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, CacheTypeList $cache)
    {
        $this->logger = $logger;
        $this->cache = $cache;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $caches = [
            "layout",
            "block_html",
            "collections",
            "reflection",
            "db_ddl",
            "compiled_config",
            "eav",
            "customer_notification",
            "config_integration",
            "config_integration_api",
            "full_page",
            "target_rule",
            "config_webservice",
            "translate",
            "checkout",
            "amasty_shopby",
            "vertex"
        ];

        foreach ($caches as $cache) {
            $this->cache->invalidate($cache);
            $this->cache->cleanType($cache);
        }
    }
}